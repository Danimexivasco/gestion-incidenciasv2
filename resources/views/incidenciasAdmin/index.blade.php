@extends('layouts.appAdmin')

@section('content')
<div class="container">

    <div id="btnCrearIncidencia">

      <div class="pull-left">

        <a class="btn btn-success" href="{{ route('incidenciasAdmin.create') }}"> Crear Incidencia</a>

    </div>
    
    {{-- Mensajes de ERROR --}}
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Algo ha ido mal.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
    
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{-- Fin mensajes de ERROR --}}

    {{-- Mensajes de EXITO --}}
    @if ($message = Session::get('success'))

    <div class="alert alert-success">
      <button type="button" class="close" data-dismiss="alert">×</button>

        <p>{{ $message }}</p>

    </div>
    {{-- <img src="uploads/{{ Session::get('file') }}"> --}}
    @endif
    {{-- Fin mensajes de EXITO --}}

      {{-- Inicio Button/MODAL --}}

      {{-- 
      <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#modalRegisterForm">
            Crear Incidencia
          </button>
          
          <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold">Incidencia</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body mx-3">
            Por favor, rellene los siguientes campos:
            <br>
            <br>
            <div class="md-form mb-4">
              <i class="fas fa-user prefix grey-text"></i>
              <input type="text" id="aula" class="form-control validate" placeholder="Introduzca Nº Aula de la incidencia:">
             
            </div>
            <div class="md-form mb-4">
              <i class="fas fa-envelope prefix grey-text"></i>
              <input type="text" id="codigo" class="form-control validate" placeholder="Introduzca codigo:">
             
            </div>
    
            <div class="md-form mb-4">
              <i class="fas fa-lock prefix grey-text"></i>
              <input type="text" id="equipo" class="form-control validate" placeholder="Equipo:">
             
            </div>
    
            <div class="md-form mb-4">
                <i class="fas fa-lock prefix grey-text"></i>
                <textarea type="text" id="descripcion" class="form-control validate" placeholder="Explicanos el problema:"></textarea>
               
              </div>
          </div>
          <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-secondary btn-lg btn-block">Enviar incidencia</button>
          </div>
        </div>
      </div>
    </div> --}}
    
        {{-- Fin MODAL --}}

    </div>
    {{-- Fin espacio para btn crear --}}

    <div class="row justify-content-center">
        <div class="col-md-14">
            <div class="card">
                <div class="card-header">Tabla de Incidencias</div>

                <div class="card-body">
                   {{--  @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif --}}
                      Bienvenido {{ Auth::user()->name }}, aquí tienes las últimas incidencias
                      <div id="tabla">
                        <table class="table table-striped table-dark table-bordered table-hover">
                                <thead>
                            <tr>
                                <th scope="col" class="align-middle">ID</th>
                                <th scope="col" class="align-middle">AULA</th>
                                <th scope="col" class="align-middle">EQUIPO</th>
                                <th scope="col" class="align-middle">CÓDIGO</th>
                                <th scope="col" class="align-middle">DESCRIPCIÓN</th>
                                <th scope="col" class="align-middle">AUTOR INCIDENCIA</th>
                                <th scope="col" class="align-middle">ADMIN A CARGO</th>
                                <th scope="col" class="align-middle">FECHA</th>
                                <th scope="col" class="align-middle">ÚLTIMA MODIFICACIÓN</th>
                                <th scope="col" class="align-middle">ESTADO</th>
                                <th scope="col" class="align-middle">COMENTARIO</th>
                                <th scope="col" class="align-middle">ARCHIVOS ADJUNTOS</th>
                                <th scope="col" class="align-middle" colspan="2">OPCIONES</th>
                                {{-- <th>ESTADO</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($incidencias as $incidencia)
                              <tr>
                                <td class="align-middle">{{$incidencia->id}}</td>
                                <td class="align-middle">{{$incidencia->aula}}</td>
                                <td class="align-middle">{{$incidencia->equipo}}</td>
                                <td class="align-middle">{{$incidencia->codigo}}</td>
                                <td class="align-middle">{{$incidencia->descripcion}}</td>
                                <td class="align-middle">{{$incidencia->profesor->name}}</td>
                                <td class="align-middle">{{$incidencia->admin['name']}}</td>
                                <td class="align-middle">{{$incidencia->created_at}}</td>
                                <td class="align-middle">{{$incidencia->updated_at}}</td>
                                <td class="align-middle">
                                @switch($incidencia->estado)
                                    @case(1)
                                        <div class="enProceso">En proceso</div>
                                        @break
                                    @case(2)
                                        <div class="solucionada">Solucionada</div>
                                        @break
                                    @default
                                        <div class="enviada">Enviada</div>
                                @endswitch
                              </td>
                            <td class="align-middle">{{$incidencia->comentario}}</td>
                            <td class="align-middle">
                              @foreach (explode(',',$incidencia->archivo) as $item)
                                <a href="/almacenamiento/{{$item}}">{{$item}}</a>
                              @endforeach
                            </td>
                                <td class="align-middle">
                                  {{-- <form action="{{ route('incidenciasAdmin.destroy',$incidencia->id) }}" method="POST"> --}}
                                    {{-- <a class="btn btn-info" href="{{ route('incidencias.show',$incidencia->id) }}">Mostrar</a> --}}
                                    <div class="btnOpciones">
                                      {{-- PRUEBAS --}}
                                    {{-- <a class="btn btn-outline-warning" href="{{ route('incidenciasAdmin.edit',$incidencia->id) }}">Editar</a> --}}
                                     <a class="btn btn-outline-warning" id="editar" href="/incidenciasAdmin/{{$incidencia->id}}/editar">Editar</a> 
                                    @csrf
                                    {{-- @method('DELETE') --}}
                                    {{-- <button type="submit" class="btn btn-outline-danger">Eliminar</button> --}}
                                    <a class="btn btn-outline-danger" id="eliminar" href="/incidenciasAdmin/{{$incidencia->id}}/eliminar">Eliminar</a> 
                                  </div>
                                {{-- </form> --}}
                                </td>
                              </tr>
                            @endforeach
                        </tbody>
                        </table>
                
                    </div>
                </div>
            </div>
        </div>
    </div>
  {{--  @foreach ($collection as $item)
       
   @endforeach --}}
</div>

<div class="row d-flex justify-content-center">
  <div style="margin-top:1%;">{!! $incidencias->links() !!}</div>
</div>
@endsection
