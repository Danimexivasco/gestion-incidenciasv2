@extends('layouts.appAdmin')

@section('content')
<div class="container">

    <div id="btnCrearIncidencia">

    {{-- Mensajes de ERROR --}}
    @if ($message = Session::get('success'))

    <div class="alert alert-success">

        <p>{{ $message }}</p>

    </div>

    @endif
    {{-- Fin mensajes de ERROR --}}


    </div>
    {{-- Fin espacio para btn crear --}}

    <div class="row justify-content-center">
        <div class="col-md-14">
            <form action="/cambioUsuarios" {{-- action="{{ route('something.update', ['id' => $id]) }}" --}} method="post">
                @csrf
            <div class="card">
                <div class="card-header">Tabla de Usuarios</div>

                <div class="card-body">
                   {{--  @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif --}}
                      Bienvenido {{ Auth::user()->name }} estos son los usuarios actuales:
                      <div id="tabla">
                        <table class="table table-striped table-dark table-bordered table-hover">
                                <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">NOMBRE</th>
                                <th scope="col">EMAIL</th>
                                <th scope="col">ADMINISTRADOR</th>
                                <th scope="col">FECHA ALTA</th>
                                <th scope="col"{{--  colspan="2" --}}>PERMISOS ADMIN</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($usuarios as $usuario)
                              <tr>
                              <td name="id" value="{{$usuario->id}}">{{$usuario->id}}</td>
                              <input type="text" name="id[]" value="{{$usuario->id}}" hidden>
                                <td>{{$usuario->name}}</td>
                                <td>{{$usuario->email}}</td>
                                @if($usuario->administrador == "1")
                                    <td>Sí</td>
                                @else
                                   <td> No</td>
                                @endif 
                                <td>{{$usuario->created_at}}</td>
                                <td> {{-- @if ($usuario->administrador == "1")
                                    <input type="checkbox" name="administrador[]" value="1" checked>
                                @else
                                    <input type="checkbox" name="administrador[]"  value="0">
                                @endif --}} {{-- <input type="checkbox" name="administrador_{{$usuario->id}}" value="0" hidden> --}} 
                                @if ($usuario->administrador =="1")
                                <input type="hidden" name="administrador_{{$usuario->id}}" value="0"> 
                                <input type="checkbox" name="administrador_{{$usuario->id}}" value="1" checked> 
                                @else
                                <input type="hidden" name="administrador_{{$usuario->id}}" value="0"> 
                                <input type="checkbox" name="administrador_{{$usuario->id}}" value="1">
                                @endif
                            </td>
                              </tr>
                            @endforeach
                        </tbody>
                        </table>
                
                    </div>
                </div>
            </div>
            <div class="row d-flex justify-content-center">
            {{-- <button class="btn btn-success" style="margin-top:1%;" href="{{route('incidenciasAdmin.index')}}">Atrás</button> --}}
            <a class="btn btn-primary" style="margin-top:1%; margin-right:1%;" href="{{ route('incidenciasAdmin.index') }}"> Volver</a>
            {{-- <a class="btn btn-primary" style="margin-top:1%;" href="/cambioUsuarios"> Guardar</a> --}}
            <button type="submit" class="btn btn-primary" style="margin-top:1%;">Guardar</button>
            {{-- <button class="btn btn-success" style="margin-top:1%;" href="/cambioUsuarios">Guardar</button> --}}
            <div>
            </form>
        </div>
    </div>
</div>

<div class="row d-flex justify-content-center">
  <div style="margin-top:1%;">{!! $usuarios->links() !!}</div> 
</div>
@endsection
