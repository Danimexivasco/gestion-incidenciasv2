@extends('layouts.app')

@section('contentAdmin')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                      Bienvenido {{ Auth::user()->name }} te logueaste!
                </div>
            </div>
        </div>
    </div>
    
</div>
@endsection
