<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

   {{--  <title>{{ config('app.name', 'Laravel') }}</title> --}}
   <title>Marrones App</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    {{-- <meta name="google-signin-client_id" content="693180154583-rqtrkngc6rsj1tr7ps3bndohagl2p9t9.apps.googleusercontent.com"> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        #tabla{
            margin-top: 5%; 
            text-align: center;
            overflow-x:scroll;
        }
        #tabla tr, th, td{
            border: 2px solid black;
        }
       #btnCrearIncidencia{
           margin-bottom: 3%;
       }
       #avatar{
           border-radius: 50%;
       }
       tbody{
           text-align: center;
       }
       .btnOpciones{
        display: flex;
        justify-content: center;
       }
       .btnOpciones #editar,#eliminar{
            margin-right: 5%;
            /* line-height: 350%; */
       }
       #editar{
           margin-left: 5%;  
       }
      /*  .btnOpciones a{
            margin-right: 7%;
       }
         */
         .enviada{
          background-color: red;
          border-radius: 20%;
          vertical-align:middle;
          padding: 5px;
      }
      .enProceso{
          background-color: orange;
          border-radius: 20%;
          padding: 5px;
      }
      .solucionada{
          background-color: green;
          border-radius: 20%;
          padding: 5px;
      }
      .alert{
          margin-top:1%;
      }
      .container{
          width: 100%;
      }
      
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark shadow col-md-14">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    Marrones App
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                   {{--  <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('incidenciasAdmin.index') }}">Inicio</a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="/usuarios">Usuarios</a>
                        </li>
                    
                        <li class="nav-item">
                            <a class="nav-link" href="/incidencias">Incidencias</a>
                        </li> 
                    </ul>  --}}

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <img id="avatar" width="45px" src="{{ Auth::user()->avatar }}" alt=""> <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Salir') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
        {{-- <div>
        @yield('userNormal')
    </div> --}}
    <div>
        @yield('incidencias')
    </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script>
function countChars(obj){
    var maxLength = 300;
    var strLength = obj.value.length;
    var charRemain = (maxLength - strLength);
    
    if(charRemain < 0){
        document.getElementById("caracteresRestantes").innerHTML = '<span style="color: red;">Has excedido el número de '+maxLength+' caracteres</span>';
    }else{
        document.getElementById("caracteresRestantes").innerHTML = charRemain+' caracteres restantes';
    }
}
</script>
</body>
</html>
