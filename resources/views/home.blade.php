@extends('layouts.appProfesores')

@section('content')
<div class="container">

    <div id="btnCrearIncidencia">

      <div class="pull-left">

        <a class="btn btn-success" href="{{ route('incidenciasCrear') }}"> Crear Incidencia</a>

    </div>

    {{-- Mensajes de ERROR --}}
    @if ($message = Session::get('success'))

    <div class="alert alert-success">

        <p>{{ $message }}</p>

    </div>

    @endif
    {{-- Fin mensajes de ERROR --}}

      {{-- Inicio Button/MODAL --}}

      {{-- 
      <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#modalRegisterForm">
            Crear Incidencia
          </button>
          
          <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h4 class="modal-title w-100 font-weight-bold">Incidencia</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body mx-3">
            Por favor, rellene los siguientes campos:
            <br>
            <br>
            <div class="md-form mb-4">
              <i class="fas fa-user prefix grey-text"></i>
              <input type="text" id="aula" class="form-control validate" placeholder="Introduzca Nº Aula de la incidencia:">
             
            </div>
            <div class="md-form mb-4">
              <i class="fas fa-envelope prefix grey-text"></i>
              <input type="text" id="codigo" class="form-control validate" placeholder="Introduzca codigo:">
             
            </div>
    
            <div class="md-form mb-4">
              <i class="fas fa-lock prefix grey-text"></i>
              <input type="text" id="equipo" class="form-control validate" placeholder="Equipo:">
             
            </div>
    
            <div class="md-form mb-4">
                <i class="fas fa-lock prefix grey-text"></i>
                <textarea type="text" id="descripcion" class="form-control validate" placeholder="Explicanos el problema:"></textarea>
               
              </div>
          </div>
          <div class="modal-footer d-flex justify-content-center">
            <button class="btn btn-secondary btn-lg btn-block">Enviar incidencia</button>
          </div>
        </div>
      </div>
    </div> --}}
    
        {{-- Fin MODAL --}}

    </div>
    {{-- Fin espacio para btn crear --}}

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Tabla de Incidencias</div>

                <div class="card-body">
                   {{--  @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif --}}
                      Bienvenido {{ Auth::user()->name }} te logueaste!
                      <div id="tabla">
                        <table class="table table-striped table-dark table-bordered table-hover">
                                <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">AULA</th>
                                <th scope="col">CODIGO</th>
                                <th scope="col">EQUIPO</th>
                                <th scope="col">DESCRIPCION</th>
                                <th scope="col">AUTOR INCIDENCIA</th>
                                <th scope="col">FECHA</th>
                                <th scope="col">ÚLTIMO CAMBIO</th>
                                <th scope="col" colspan="2">OPCIONES</th>
                                {{-- <th>ESTADO</th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                              <tr>
                                <td>{{$item->id}}</td>
                                <td>{{$item->aula}}</td>
                                <td>{{$item->equipo}}</td>
                                <td>{{$item->codigo}}</td>
                                <td>{{$item->descripcion}}</td>
                                <td>{{$item->profesor_id}}</td>
                                <td>{{$item->created_at}}</td>
                              <td>{{$item->updated_at}}</td>
                                {{-- <td><button class="btn btn-outline-warning">Modificar</button></td>
                                <td><button class="btn btn-outline-danger">Eliminar</button></td> --}}
                                <td>
                                  <form action="{{ route('incidencias.eliminar',$incidencia->id) }}" method="POST">
                                    <a class="btn btn-info" href="{{ route('incidenciasShow',$incidencia->id) }}">Mostrar</a>
                                    <a class="btn btn-outline-warning" href="{{ route('incidenciasEditar',$incidencia->id) }}">Editar</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-outline-warning">Eliminar</button>
                                </form>
                                </td>
                              </tr>
                            @endforeach
                        </tbody>
                        </table>
                
                    </div>
                </div>
            </div>
        </div>
    </div>
  {{--  @foreach ($collection as $item)
       
   @endforeach --}}
</div>
@endsection
