@extends('layouts.appProfesores')

@section('incidencias')
<div class="container">
    {{-- <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div> --}}


    {{-- Inicio Button/MODAL --}}

    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalRegisterForm">
        Meter Incidencia
      </button>
      
      <div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Incidencia</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        Por favor, rellene los siguientes campos:
        <br>
        <br>
        <div class="md-form mb-4">
          <i class="fas fa-user prefix grey-text"></i>
          <input type="text" id="aula" class="form-control validate" placeholder="Introduzca Nº Aula de la incidencia:">
          {{-- <label data-error="wrong" data-success="right" for="aula">Aula</label> --}}
        </div>
        <div class="md-form mb-4">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="text" id="codigo" class="form-control validate" placeholder="Introduzca codigo:">
          {{-- <label data-error="wrong" data-success="right" for="codigo">Codigo</label> --}}
        </div>

        <div class="md-form mb-4">
          <i class="fas fa-lock prefix grey-text"></i>
          <input type="text" id="equipo" class="form-control validate" placeholder="Equipo:">
          {{-- <label data-error="wrong" data-success="right" for="equipo">Equipo</label> --}}
        </div>

        <div class="md-form mb-4">
            <i class="fas fa-lock prefix grey-text"></i>
            <textarea type="text" id="descripcion" class="form-control validate" placeholder="Explicanos el problema:"></textarea>
            {{-- <label data-error="wrong" data-success="right" for="descripcion">Descripcion</label> --}}
          </div>
      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-secondary btn-lg btn-block">Enviar incidencia</button>
      </div>
    </div>
  </div>
</div>

    {{-- Fin MODAL --}}
    <div id="container">
    <div id="tabla">
        <table>

            <tr>
                <th>ID</th>
                <th>AULA</th>
                <th>CODIGO</th>
                <th>EQUIPO</th>
                <th>DESCRIPCION</th>
                <th>AUTOR INCIDENCIA</th>
                {{-- <th>ESTADO</th> --}}
            </tr>
            @foreach ($data as $item->$dato)
                <td>{{$dato->id}}</td>
                <td>{{$dato->aula}}</td>
                <td>{{$dato->codigo}}</td>
                <td>{{$dato->equipo}}</td>
                <td>{{$dato->descripcion}}</td>
                <td>{{$dato->id_profesor}}</td>
            @endforeach
        </table>

    </div>
  </div>
</div>
@endsection