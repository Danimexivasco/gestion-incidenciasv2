
<p>Buenas, soy {{$data['nombre']}}</p>
<p>Te escribo en respuesta de la incidencia: 
    
    <ul>
        <li>Problema: <strong>  {{ $data['codigo'] }}</strong></li>
        <li>Estado: @switch($data['estado'])
            @case(1)
                En proceso
                @break
            @case(2)
                Solucionada
                @break
            @default
                Enviada
        @endswitch</li>
        <li>Equipo: {{$data['equipo']}}</li>
        <li>Descripcion: {{$data['descripcion']}}</li>
    </ul>
<p>En base a lo anterior mi diagnostico es el siguiente:</p>
<p><i>{{$data['comentario']}}</i></p>

