@extends('layouts.appProfesores')

@section('incidencias')
<div class="container">
    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Editar incidencia</h2>

            </div>

            

        </div>

    </div>

   

    @if ($errors->any())

        <div class="alert alert-danger">

            <strong>Whoops!</strong> Has introducido mal algún dato.<br><br>

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif

  

    <form action="{{ route('incidencias.update',$incidencia->id) }}" method="POST">

        @csrf

        @method('PUT')

   

         <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Aula:</strong>

                    <input type="text" name="aula" value="{{ $incidencia->aula }}" class="form-control" placeholder="Introduce el aula:">

                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <label for="inputState"><strong>Codigo:</strong></label>
                <select id="inputState" class="form-control" name="codigo">
                    <option value="{{$incidencia->codigo}}"  selected hidden>{{$incidencia->codigo}}</option>
                  {{-- <option selected>Choose...</option> --}}
                  <option value="No se enciende la CPU">1- No se enciende la CPU</option>
                  <option value="No se enciende la pantalla">2- No se enciende la pantalla</option>
                  <option value="No entra en mi sesión">3- No entra en mi sesión</option>
                  <option value="No navega en internet">4- No navega en internet</option>
                  <option value="No se oye el sonido">5- No se oye el sonido</option>
                  <option value="No lee CD/DVD">6- No lee CD/DVD</option>
                  <option value="Teclado roto">7- Teclado roto</option>
                  <option value="No funciona el ratón">8- No funciona el ratón</option>
                  <option value="Funcionamiento muy lento">7- Funcionamiento muy lento</option>
                  <option value="">8- Otros, especifíca</option>
                </select>
              </div>
            </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12" style="max-width:100%;padding-left: 0px;padding-right: 0px;">

                <div class="form-group">

                    <strong>Equipo:</strong>

                    <input type="text" name="equipo" value="{{ $incidencia->equipo }}" class="form-control" placeholder="Introduce el equipo:">

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">

                <div class="form-group">

                    <strong>Descripcion:</strong>

                    <textarea class="form-control" style="height:150px" name="descripcion" maxlength="300" onkeyup="countChars(this);" placeholder="Describe el problema...">{{ $incidencia->descripcion }}</textarea>
                    <p id="caracteresRestantes" name="caracteresRestantes"></p>
                </div>

            </div>
            
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a class="btn btn-primary" href="{{ route('incidencias.index') }}"> Volver</a>
                <button type="submit" class="btn btn-primary">Guardar</button>      
            </div>

        </div>

   

    </form>
</div>
@endsection