@extends('layouts.appProfesores')

@section('incidencias')
<div class="container">
    <div class="row">

        <div class="col-lg-12 margin-tb">
    
            <div class="pull-left">
    
                <h2>Crear nueva Incidencia</h2>
    
            </div>
    
        </div>
    
    </div>
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Has metido algún dato erroneo.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
    
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    <form action="{{ route('incidencias.store') }}"  method="POST" enctype="multipart/form-data">
        @csrf
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Aula:</strong>
                    <input type="text" name="aula" class="form-control" value="{{old('aula')}}" placeholder="Introduce el aula en la que se encuentra el problema:">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <label for="inputState"><strong>Codigo:</strong></label>
                <select id="inputState" class="form-control" name="codigo">
                    <option value="" disabled selected hidden>Eligue...</option>
                  {{-- <option selected>Choose...</option> --}}
                  <option value="No se enciende la CPU">1- No se enciende la CPU</option>
                  <option value="No se enciende la pantalla">2- No se enciende la pantalla</option>
                  <option value="No entra en mi sesión">3- No entra en mi sesión</option>
                  <option value="No navega en internet">4- No navega en internet</option>
                  <option value="No se oye el sonido">5- No se oye el sonido</option>
                  <option value="No lee CD/DVD">6- No lee CD/DVD</option>
                  <option value="Teclado roto">7- Teclado roto</option>
                  <option value="No funciona el ratón">8- No funciona el ratón</option>
                  <option value="Funcionamiento muy lento">7- Funcionamiento muy lento</option>
                  <option value="Otros">8- Otros, especifíca en la descripción</option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Equipo:</strong>
                    <input type="text" name="equipo" value="{{old('equipo')}}" class="form-control" placeholder="Equipo: Ej. HZ321546">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Descripción: </strong> (máximo 300 carácteres)
                    <textarea class="form-control" style="height:150px" name="descripcion" maxlength="300" onkeyup="countChars(this);" placeholder="Describe el problema...">{{old('descripcion')}}</textarea>
                    <p id="caracteresRestantes" name="caracteresRestantes"></p>
                </div>
            </div>
            
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <strong>Subir Archivo:</strong>
                        <input type="file" name="archivo[]" class="form-control-file" aria-describedby="descrAyuda" multiple>
                        <small id="descrAyuda" class="form-text text-muted">Archivos soportados: <i>.pdf .doc .csv .jpeg .png .txt .ods (2MB máx. por archivo)</i> </small>
                    </div>
                </div>
                

        {{-- <input type="text" name="profesor_id" class="form-control" value="{{Auth::user()->id}}" hidden> --}}
        
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <a class="btn btn-primary" href="{{ route('incidencias.index') }}"> Atrás</a>
                <button type="submit" class="btn btn-primary">Crear</button>
            </div>
        
        
    </form>
</div>

@endsection