<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncidenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incidencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('aula');
            $table->string('codigo');
            $table->string('equipo');
            $table->longText('descripcion');
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->foreign('admin_id')->references('id') -> on('profesores')
            ->onDelete('cascade'); 
            $table->unsignedBigInteger('profesor_id');
            $table->foreign('profesor_id')->references('id') -> on('profesores')
            ->onDelete('cascade');
            $table->timestamps();
        });

                

    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incidencias');
    }
}
