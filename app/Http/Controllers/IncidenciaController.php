<?php

namespace App\Http\Controllers;

use App\Incidencia;
use App\Profesor;
use Illuminate\Http\Request;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\EnvioMail;
use League\Flysystem\File;
use Illuminate\Support\Facades\Storage;

use Symfony\Component\Console\Input\Input;




class IncidenciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*  $incidencias = DB::table('incidencias')->where('id',Auth::user()->id)->paginate(5); */
        /* $incidencias = Incidencia::latest()->paginate(5); */
        $incidencias = Incidencia::where('profesor_id', Auth::user()->id)->latest()->paginate(5);
        return view('incidencias.index', compact('incidencias'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('incidencias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'aula' => 'required | integer | lte:999',
            'codigo' => 'required | in:No se enciende la CPU,No se enciende la pantalla,No entra en mi sesión,No navega en internet,No se oye el sonido,No lee CD/DVD,Teclado roto,No funciona el ratón,Funcionamiento muy lento,Otros',
            'equipo' => ['required', 'regex:/(HZ[0-9]{6})/u', 'max:8', 'min:8'],
            'descripcion' => 'required | string',
            'archivo.*' => 'mimes:doc,docx,pdf,txt,jpeg,png,ods,xls,xlsx,zip,rar | max:2048',
        ]);

        $adminsMail = DB::table('profesores')->select('email')->where('administrador', true)->get();

        $data = array(
            'aula' => $request->aula,
            'codigo' => $request->codigo,
            'equipo' => $request->equipo,
            'descripcion' => $request->descripcion,
            'email' => Auth::user()->email,
            'nombre' => Auth::user()->name,
            'admins' => $adminsMail,
        );


        /* error_log($adminsMail); */
        /* foreach ($adminsMail as $admin){ */
        Mail::to($adminsMail)->send(new EnvioMail($data));
        /* }; */

        /* $fileName = time().'.'.$request->file->getClientOriginalExtension();  
         $request->file->move(public_path('uploads'), $fileName); */

        /* Otra opcion de ponerle nombre */
        /* $file_route = date('YmdHis') . "." . $file->getClientOriginalExtension(); */
        /* $file = $request->file('archivo');
       
            $destinationPath = 'public/archivos/'; // upload path
            
            $file_route = time() . "_" . $file->getClientOriginalName();
            $file->move($destinationPath, $file_route);
            $insert['archivo'] = "$file_route";  */

        /*  $archivo = $request->file('archivo');
        if ($archivo) {
            $ruta_archivo = time() . "_" . $archivo->getClientOriginalName();
            Storage::disk('archivos_subidos')->put($ruta_archivo, file_get_contents($archivo->getRealPath())); */


        $archivo = $request->file('archivo');
        $arrArchivos = array();
        if ($archivo) {
            foreach ($archivo as $file) {
                $ruta_archivo = time() . "_" . $file->getClientOriginalName();
                array_push($arrArchivos, $ruta_archivo);
                Storage::disk('archivos_subidos')->put($ruta_archivo, file_get_contents($file->getRealPath()));
                error_log($file->getClientOriginalName());
                error_log($ruta_archivo);
            }
        }
        /* error_log($archivo->getClientOriginalName());
            error_log($ruta_archivo); */
        /* error_log($destinationPath); */
        /* } */
        $ruta = implode(',', $arrArchivos);

        $incidencia = new Incidencia();

        $incidencia->aula = $request->aula;
        $incidencia->codigo = $request->codigo;
        $incidencia->equipo = $request->equipo;
        $incidencia->descripcion = $request->descripcion;
        $incidencia->profesor_id = Auth::user()->id;
        /* $incidencia->archivo = $ruta_archivo; */
        $incidencia->archivo = $ruta;

        $incidencia->save();


        /* Incidencia::create($request->all());  // O mediante el metodo save() */


        return redirect()->route('incidencias.index')
            ->with('success', 'Incidencia creada correctamente, se le envio un mail notificando al admin.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function show(Incidencia $incidencia)
    {
        return view('incidencias.show', compact('incidencia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function edit(Incidencia $incidencia)
    {

        $user = Auth::user();
        if ($user->can('edit', $incidencia)) {
            return view('incidencias.edit', compact('incidencia'));
        } else {
            return redirect()->route('incidencias.index')
                ->withErrors(['ERROR. No eres propietario de la incidencia que deseas editar']);
        }

        /*  */
        /* if ((Auth::user()->id == $incidencia->profesor_id) || (Auth::user()->administrador=="1")) {
            return view('incidencias.edit', compact('incidencia'));
        } else {
            return redirect()->route('incidencias.index')
            ->withErrors(['ERROR. No eres propietario de la incidencia que deseas editar']);
        } */

        /*  return view('incidencias.edit', compact('incidencia')); */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Incidencia $incidencia)
    {
        $user = Auth::user();
        if ($user->can('update', $incidencia)) {
            $request->validate([
                'aula' => 'required | integer | lte:999',
                'codigo' => 'required | in:No se enciende la CPU,No se enciende la pantalla,No entra en mi sesión,No navega en internet,No se oye el sonido,No lee CD/DVD,Teclado roto,No funciona el ratón,Funcionamiento muy lento,Otros',
                'equipo' => ['required', 'regex:/(HZ[0-9]{6})/u', 'max:8', 'min:8'],
                'descripcion' => 'required | string',
                'archivo.*' => 'mimes:doc,docx,pdf,txt,jpeg,png,ods,xls,xlsx,zip,rar | max:2048',
            ]);

            /* $incidencia->aula = $request->aula;
            $incidencia->codigo = $request->codigo;
            $incidencia->equipo = $request->equipo;
            $incidencia->descripcion = $request->descripcion;
            //$incidencia->profesor_id = Auth::user()->id;

            $incidencia->save(); */
            $incidencia->update($request->all());
            return redirect()->route('incidencias.index')
                ->with('success', 'Incidencia actualizada satisfactoriamente!');
        } else {
            return redirect()->route('incidencias.index')
                ->withErrors(['ERROR. No eres propietario de la incidencia que deseas actualizar']);
        }

        /* if ((Auth::user()->id == $incidencia->profesor_id) || (Auth::user()->administrador=="1")) {
            $request->validate([
                'aula' => 'required | integer | lte:999',
                'codigo' => 'required | in:No se enciende la CPU,No se enciende la pantalla,No entra en mi sesión,No navega en internet,No se oye el sonido,No lee CD/DVD,Teclado roto,No funciona el ratón,Funcionamiento muy lento,Otros',
                'equipo' => ['required', 'regex:/(HZ[0-9]{6})/u', 'max:8', 'min:8'],
                'descripcion' => 'required | string',
                'archivo.*' => 'mimes:doc,docx,pdf,txt,jpeg,png,ods,xls,xlsx,zip,rar | max:2048',
            ]);
            $incidencia->update($request->all());
            return redirect()->route('incidencias.index')
                ->with('success', 'Incidencia actualizada satisfactoriamente!');
        } else {
            return redirect()->route('incidencias.index')
            ->withErrors(['ERROR. No eres propietario de la incidencia que deseas actualizar']);
        } */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Incidencia $incidencia)
    {

        $user = Auth::user();
        if ($user->can('delete', $incidencia)) {
            $incidencia->delete();
            return redirect()->route('incidencias.index')
                ->with('success', 'Incidencia eliminada satisfactoriamente!');
        } else {
            return redirect()->route('incidencias.index')
                // ->withErrors(['Error al eliminar', 'No eres propietario de la incidencia']); 
                ->withErrors(['No eres propietario de la incidencia que deseas eliminar']);
        }

        /*  if ((Auth::user()->id == $incidencia->profesor_id) || (Auth::user()->administrador=="1")) {
            $incidencia->delete();
            return redirect()->route('incidencias.index')
                ->with('success', 'Incidencia eliminada satisfactoriamente!');
        } else {
            return redirect()->route('incidencias.index')
            // ->withErrors(['Error al eliminar', 'No eres propietario de la incidencia']); 
            ->withErrors(['No eres propietario de la incidencia que deseas eliminar']);
        } */
    }
}
