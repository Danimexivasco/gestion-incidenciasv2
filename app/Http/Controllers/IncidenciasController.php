<?php

namespace App\Http\Controllers;

use App\Incidencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IncidenciasController extends Controller
{
   /*  public function index(){
 
        $devices = Device::all();
 
        return view('devices.index',compact('devices'));
    } */

   /*  public function index(){
        $incidencias = Incidencia::latest()->paginate(5);
        return view('incidencias.index',compact('incidencias'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    } */
 
    public function mostrar(){
/* 
        $incidencias = DB::table('incidencias')->get();
        return view('incidencias',['data'=> $incidencias]); */

        $incidencias = Incidencia::latest()->paginate(5);
        return view('incidencias',compact('incidencias'))
            ->with('i', (request()->input('page', 1) - 1) * 5);       
    }


    public function crear(){
        return view('incidenciasCrear');
    }
 
    public function guardarIncidencia(){
 
        $incidencia = new Incidencia();
 
        $incidencia->aula = request('aula');
        $incidencia->codigo = request('codigo');
        $incidencia->equipo = request('equipo');
        $incidencia->descripcion = request('descripcion');
        $incidencia->save();
 
        return redirect('/incidencias');
 
    }

    public function show(Incidencia $incidencia){
        return view('incidenciasShow',compact('incidencia'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */


    public function editar(Incidencia $incidencia){
        return view('incidenciasEditar',compact('incidencia'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Incidencia $incidencia){
        $request->validate([
            'aula' => 'required',
            'codigo' => 'required',
            'equipo' => 'required',
            'descripcion'=>'required',

        ]);
        $incidencia->update($request->all());
        return redirect()->route('incidencias')
                        ->with('success','Incidencia actualizada satisfactoriamente!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */


    public function eliminar (Incidencia $incidencia){
        $incidencia->delete();
        return redirect()->route('incidencias')
                        ->with('success','Incidencia eliminada satisfactoriamente!');
    }
}
