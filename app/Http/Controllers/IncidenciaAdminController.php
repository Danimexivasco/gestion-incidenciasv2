<?php

namespace App\Http\Controllers;

use App\Incidencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\EnvioMail;
use Illuminate\Support\Facades\Storage;


class IncidenciaAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $incidencias = Incidencia::latest()->paginate(5);
        return view('incidenciasAdmin.index',compact('incidencias'))
            ->with('i', (request()->input('page', 1) - 1) * 5);  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('incidenciasAdmin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'aula' => 'required | integer | lte:999',
            'codigo' => 'required | in:No se enciende la CPU,No se enciende la pantalla,No entra en mi sesión,No navega en internet,No se oye el sonido,No lee CD/DVD,Teclado roto,No funciona el ratón,Funcionamiento muy lento,Otros',
            'equipo' => ['required', 'regex:/(HZ[0-9]{6})/u', 'max:8', 'min:8'],
            'descripcion' => 'required | string',
            'estado'=>'required | in:0,1,2',
            'archivo.*' => 'mimes:doc,docx,pdf,txt,jpeg,png,ods,xls,xlsx,zip,rar | max:2048',
        ]);
        $adminsMail = DB::table('profesores')->select('email')->where('administrador',true)->get();
        /* $adminsMail = $adminsMail ->toArray(); */
        /* $array = json_decode($adminsMail, true);
        $string = $array ->toString(); */
        /* $area = json_decode($adminsMail, true);

            foreach($area['email'] as $i => $v)
            {
                echo $v['area'].'<br/>';
            } */

        $data = array( 'aula' => $request->aula,
        'codigo' => $request->codigo,
        'equipo' => $request->equipo,
        'descripcion'=>$request->descripcion,
        'email' => Auth::user()->email,
        'nombre' => Auth::user()->name,
        'admins' =>$adminsMail,);
     
        /* foreach($adminsMail as $admin){
            error_log($admin->email); 
        } */
        
        /* trigger_error($string); */
        /* foreach ($adminsMail as $admin){ 
        Mail::to($admin->email)->send(new EnvioMail($data));
        };  */
        Mail::to($adminsMail)->send(new EnvioMail($data));

        $archivo = $request->file('archivo');
        $arrArchivos = array();
        if ($archivo) {
            foreach ($archivo as $file) {
                $ruta_archivo = time() . "_" . $file->getClientOriginalName();
                array_push($arrArchivos, $ruta_archivo);
                Storage::disk('archivos_subidos')->put($ruta_archivo, file_get_contents($file->getRealPath()));
                error_log($file->getClientOriginalName());
                error_log($ruta_archivo);
            }
        }
        else{
            error_log("no entra");
        }
        
        $ruta = implode(',', $arrArchivos);

        $incidencia = new Incidencia();

        $incidencia->aula = $request->aula;
        $incidencia->codigo = $request->codigo;
        $incidencia->equipo = $request->equipo;
        $incidencia->estado = $request->estado;
        $incidencia->descripcion = $request->descripcion;
        $incidencia->profesor_id = Auth::user()->id;
        /* $incidencia->archivo = $ruta_archivo; */
        $incidencia->archivo = $ruta;

        $incidencia->save();

        /* Incidencia::create($request->all()); */
        return redirect()->route('incidenciasAdmin.index')
            ->with('success','Incidencia creada correctamente, se le envio un mail notificando al admin.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function show(Incidencia $incidencia)
    {
        return view('incidenciasAdmin.show',compact('incidencia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function edit(Incidencia $incidencia)  //En AdminManualController
    {
        if ((Auth::user()->id == $incidencia->profesor_id) || (Auth::user()->administrador=="1")) {
            return view('incidenciasAdmin.edit',compact('incidencia'));
        } else {
            return redirect()->route('incidencias.index')
            ->withErrors(['ERROR. No eres propietario de la incidencia que deseas editar']);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Incidencia $incidencia)    //En AdminManualController
    {
        $request->validate([
            'aula' => 'required | integer | lte:999',
            'codigo' => 'required | in:No se enciende la CPU,No se enciende la pantalla,No entra en mi sesión,No navega en internet,No se oye el sonido,No lee CD/DVD,Teclado roto,No funciona el ratón,Funcionamiento muy lento,Otros',
            'estado'=>'required | in:0,1,2',
            'equipo' => ['required', 'regex:/(HZ[0-9]{6})/u', 'max:8', 'min:8'],
            'descripcion' => 'required | string',
            'archivo.*' => 'mimes:doc,docx,pdf,txt,jpeg,png,ods,xls,xlsx,zip,rar | max:2048',
        ]);

        // $incidencia->aula = $request->aula;
        // $incidencia->codigo = $request->codigo;
        // $incidencia->equipo = $request->equipo;
        // $incidencia->estado = $request->estado;
        // $incidencia->descripcion = $request->descripcion;
        // $incidencia->profesor_id = $request->profesor_id;
        // $incidencia->admin_id = Auth::user();
        
        

        // $incidencia->save();
        $incidencia->update($request->all());
        return redirect()->route('incidenciasAdmin.index')
            ->with('success','Incidencia actualizada satisfactoriamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function destroy(Incidencia $incidencia) //En AdminManualController
    {
        $incidencia->delete();
        return redirect()->route('incidenciasAdmin.index')
                        ->with('success','Incidencia eliminada satisfactoriamente!');
    }
}
