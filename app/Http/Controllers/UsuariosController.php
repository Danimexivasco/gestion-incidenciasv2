<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profesor;
/* use Illuminate\Support\Facades\Request; */
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;





class UsuariosController extends Controller
{
    public function usuarios()
    {
        $usuarios = Profesor::latest()->paginate(10);
        return view('usuarios', compact('usuarios'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function guardar(Request $request, Profesor $profesor)
    {
        foreach ($request->id as $key => $value) {
            $admins = Profesor::find($request->id[$key]);
            $admins->administrador = $request->input('administrador_' . $request->id[$key]);
            $admins->update();
        }


        /*  dd($request->input('administrador_'. 1)); */

        /* $ar = $request->administrador
        foreach ($profesor as $prof) {

            $prof->administrador = $request->has('administrador' . $prof->id) ? 1:0;
            
            $prof->update();
        } */
        /* Profesor::whereNotIn('id', $request->id)->update(['administrador' => 0]); */

        /* $admins->administrador = data_get($request -> input('administrador')[$key], 1, 0); */


        return redirect()->route('incidenciasAdmin.index')
            ->with('success', 'Se cambio de tipo de usuario correctamente!');
    }

    public function verArchivo($archivo)
    {

        /* $ruta = public_path('public/archivos_subidos/' . $archivo);

            if (!File::exists($ruta)) {
                abort(404);
            }

            $file = File::get($ruta);
            $type = File::mimeType($ruta);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);

            return $response;  */


        /* return response()->download('app/public/archivos_subidos/' . $archivo);   */

        /*  $ruta = app_path().'/public/archivos_subidos/' . $archivo;
       return Storage::download($ruta); */

        
        
        return Storage::disk('archivos_subidos')->download($archivo);
        
    }
}
