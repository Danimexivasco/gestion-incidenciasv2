<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Incidencia;
use App\Profesor;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\EnvioProfesor;

class AdminManualController extends Controller
{
    public function mostrarTabla(Request $request){

        $incidencias = DB::table('incidencias')->get();
        return view('home',['data' => $incidencias]);
    }

    public function editar(Request $request, $id)
    {
        /* error_log("incidenciasAdmin/{{$incidencia->id}}/edit");  */
        $incidencia = Incidencia::find($id);
        $profesores = Profesor::all();
        $request->flash();
        return view('editar',compact(['incidencia', 'profesores']));

       /*  dd($incidencia); */
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function actualizar(Request $request, $id)
    {
        $incidencia = Incidencia::find($id);
        $request->validate([
            'aula' => 'required',
            'codigo' => 'required',
            'equipo' => 'required',
            'descripcion'=>'required',
            'estado' => 'required',
            'admin_id' => 'required',
            'profesor_id' =>'required',
        ]);
        $incidencia->update($request->all());

        $mailProfesor = DB::table('profesores')->select('email')->where('id',$request->profesor_id)->get();

        $data = array( 'aula' => $request->aula,
        'codigo' => $request->codigo,
        'equipo' => $request->equipo,
        'descripcion'=>$request->descripcion,
        'email' => Auth::user()->email,
        'nombre' => Auth::user()->name,
        'profe' =>$mailProfesor,
        'estado' => $request->estado,
        'comentario'=>$request->comentario);
     
        
        error_log($mailProfesor);
       
        Mail::to($mailProfesor)->send(new EnvioProfesor($data));
        

        return redirect()->route('incidenciasAdmin.index')
            /* ->withInput() */
            ->with('success','Incidencia actualizada satisfactoriamente!');
            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Incidencia  $incidencia
     * @return \Illuminate\Http\Response
     */
    public function eliminar(Incidencia $incidencia, $id)
    {
        $incidencia = Incidencia::find($id);
        $incidencia->delete();
        return redirect()->route('incidenciasAdmin.index')
                        ->with('success','Incidencia eliminada satisfactoriamente!');
    }
}
