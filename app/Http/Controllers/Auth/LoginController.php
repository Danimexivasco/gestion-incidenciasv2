<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Profesor;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Util\Exception;
use Symfony\Component\HttpFoundation\Request;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    /* protected $redirectTo = '/home'; */
    protected $redirectTo = 'incidencias.index';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function redirectToGoogle()
{
    return Socialite::driver('google')->redirect();
}
public function handleGoogleCallback()
    {
        try {
  
            $user = Socialite::driver('google')->stateless()->user();
            /* error_log($user->token); */

            // Condicional para el Login de usuario plaiaundi
            if (strpos($user->email, 'plaiaundi.net') || strpos($user->email, 'plaiaundi.com')) {

                $finduser = Profesor::where('google_id', $user->id)->first();
    
                if($finduser){
    
                    Auth::login($finduser);
    
                    /* return redirect('/home'); */
                    if(Auth::user()->administrador == '1'){
                        return redirect()->route('incidenciasAdmin.index');
                    }
                    else{
                        return redirect()->route('incidencias.index');
                    }
                    /* return redirect()->route('incidencias.index'); */
    
                }else{
                    
                    $newUser = Profesor::create([
                        'name' => $user->name,
                        'email' => $user->email,
                        'google_id'=> $user->id,
                        'avatar' => $user->avatar,
                    ]);
    
                    Auth::login($newUser);
    
                    return redirect()->back();
                }
       /*  }elseif (strpos($user->email, 'plaiaundi.net')) {
            //expose(@, ....)
            
                $newUser = Profesor::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'avatar' => $user->avatar,
                ]);
    
                Auth::login($newUser);
    
                return redirect()->back();
            } */

            // Else del condicional del usuario plaiaundi.net
        }else{
                return redirect('/errorLogin');
            } 
        
        } catch (Exception $e) {
            return redirect('auth/google');
            /* return redirect('/errorLogin'); */
        }
    }

    public function errorLogin(Request $request){
        
        $request->session()->flush();
        $request->session()->regenerate();
        return view('errorLogin');
    }
}


/*  try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect('/login');
        }        // only allow people with @company.com to login
        if(explode("@", $user->email)[1] !== 'plaiaundi.net'){
            return redirect()->to('/');
        }        // check if they're an existing user
        $existingUser = User::where('email', $user->email)->first();       
         if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            // create a new user
            $newUser                  = new User;
            $newUser->name            = $user->name;
            $newUser->email           = $user->email;
            $newUser->google_id       = $user->id;
            //$newUser->avatar          = $user->avatar;
            //$newUser->avatar_original = $user->avatar_original;
            $newUser->save();            auth()->login($newUser, true);
        }
        return redirect()->to('/home'); */