<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
           /*  return redirect('/home'); */
           return redirect()->route('incidencias.index');
        }
        if (Auth::guard($guard)->check() && Auth::user()->administrador == '1') {
            return redirect()->route('incidenciasAdmin.index');
         }

        return $next($request);
    }
}
