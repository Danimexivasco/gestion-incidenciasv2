<?php

namespace App;

use Illuminate\Foundation\Auth\User as Autenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;



class Profesor extends Autenticatable
{

    use Notifiable;
    protected $table = "profesores";
    protected  $primaryKey = 'id';
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',/* 'provider', 'provider_id', */ 'google_id','avatar','avatar_original','administrador',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
