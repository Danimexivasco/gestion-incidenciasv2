<?php

namespace App\Policies;

use App\Profesor;
use App\Incidencia;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermisosPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any profesors.
     *
     * @param  \App\Profesor  $user
     * @return mixed
     */
    public function viewAny(Profesor $user)
    {
        //
    }

    /**
     * Determine whether the user can view the profesor.
     *
     * @param  \App\Profesor  $user
     * @param  \App\Profesor  $profesor
     * @return mixed
     */
    public function view(Profesor $user, Profesor $profesor)
    {
        //
    }

    /**
     * Determine whether the user can create profesors.
     *
     * @param  \App\Profesor  $user
     * @return mixed
     */
    public function create(Profesor $user)
    {
        //
    }

    /**
     * Determine whether the user can update the profesor.
     *
     * @param  \App\Profesor  $user
     * @param  \App\Profesor  $profesor
     * @return mixed
     */
    public function update(Profesor $user, Profesor $profesor)
    {
        //
    }

    /**
     * Determine whether the user can delete the profesor.
     *
     * @param  \App\Profesor  $user
     * @param  \App\Profesor  $profesor
     * @return mixed
     */
    public function delete(Profesor $user, Profesor $profesor)
    {
        //
    }

    /**
     * Determine whether the user can restore the profesor.
     *
     * @param  \App\Profesor  $user
     * @param  \App\Profesor  $profesor
     * @return mixed
     */
    public function restore(Profesor $user, Profesor $profesor)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the profesor.
     *
     * @param  \App\Profesor  $user
     * @param  \App\Profesor  $profesor
     * @return mixed
     */
    public function forceDelete(Profesor $user, Profesor $profesor)
    {
        //
    }
}
