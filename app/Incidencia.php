<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incidencia extends Model
{
    protected $table = 'incidencias';
    
    protected $fillable = [
        'aula', 'codigo', 'equipo', 'descripcion', 'profesor_id', 'estado', 'admin_id', 'comentario', 'archivo',
    ];
    
    /* public function admin(){
        return $this-> belongsTo(Admin::class);
    } */
    public function profesor(){
        return $this-> belongsTo(Profesor::class);
    }
    public function admin(){
      return $this->belongsTo(Profesor::Class,'admin_id');
    }
}
