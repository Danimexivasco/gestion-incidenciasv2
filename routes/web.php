<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\UsuariosController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
/* Route::get('/home', 'HomeController@index')->name('home'); */

Route::get('errorLogin','Auth\LoginController@errorLogin');

/* 
Route::get('/redirect', 'Auth\LoginController@redirectToProvider');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback'); */

/* Route::get('google', function () {
    return view('googleAuth');
}); */
   
/* Route::get('auth/google', 'Auth\LoginController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\LoginController@handleGoogleCallback'); */

/* SOCIALITE GOOGLE */

Route::get('auth/google', 'Auth\LoginController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\LoginController@handleGoogleCallback');

/* FIN SOCIALITE */

/* RUTAS INCIDENCIAS */

Route::resource('incidencias','IncidenciaController');

Route::resource('incidenciasAdmin','IncidenciaAdminController')->middleware('administrador');

Route::get('/usuarios','UsuariosController@usuarios');
Route::post('/cambioUsuarios','UsuariosController@guardar')/* ->name('something.update') */;

/* Route::get('/incidencias','IncidenciasController@mostrar');

Route::get('/incidenciasCrear','IncidenciasController@crear'); 
Route::post('/guardarIncidencia','IncidenciasController@guardarIncidencia'); */

/* FIN INCIDENCIAS */

/* RUTAS MANUALES EDIT / UPDATE / DELETE  */

Route::get('/incidenciasAdmin/{id}/editar','AdminManualController@editar');
Route::post('/incidenciasAdmin/{id}/actualizar','AdminManualController@actualizar');
Route::get('/incidenciasAdmin/{id}/eliminar', 'AdminManualController@eliminar');

/* Fin rutas MANUALES */

Route::get('/almacenamiento/{archivo}','UsuariosController@verArchivo');
